import { Component, OnInit } from '@angular/core';
import { EventService } from '../services/event.service';
import { Event } from '../entity/event';
import { Observable } from 'rxjs';

@Component({
  selector: 'simplon-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  // events: Event[] = [];
  events: Observable<Event[]>;
  newEvent: Event = {label:"", date:""};
  selected:Event;

  constructor(private service:EventService) { }

  ngOnInit() {
    // this.service.findAll().subscribe(data => this.events = data);
    this.events = this.service.findAll();
  }
  /**
   * Méthode qui vérifie si une date est inférieure à la date
   * du jour ou non
   * @param date date à vérifier
   */
  isPassed(date:Date) {
    let now = new Date();
    
    return date.getTime() < now.getTime();
  }
  
 addEvent(){
   this.service.add(this.newEvent)
   .subscribe( () => this.ngOnInit() );
   
 }

 deleteEvent() {
   this.service.delete(this.selected.id)
   .subscribe(() => {
     this.ngOnInit();
     this.selected = null;
   });
 }
 
}
