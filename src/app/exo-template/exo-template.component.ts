import { Component } from "@angular/core";

@Component({
    templateUrl: './exo-template.component.html',
    selector: 'simplon-exo-template'
})
export class ExoTemplateComponent {
    myColor = 'black';
    
    chngColor(newColor)
    {
        this.myColor = newColor;
    }
}