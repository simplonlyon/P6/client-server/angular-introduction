export interface Picture {
    id?:number;
    description:string;
    url:string;
    date?:string;
}
