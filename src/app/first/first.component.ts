import { Component } from "@angular/core";

/**
 * Un Component angular représente une brique d'interface utilisateur
 * qui pourra être réutilisée dans notre application (ou d'autres appli)
 * Un Component doit être décorée par une annotation @Component et doit
 * déclarer obligatoirement un template (soit directement en string 
 * dans la classe soit dans un fichier à part avec le templateUrl), et
 * un selector qui représente la balise html qui fera appel à ce component.
 */
@Component({
    templateUrl: './first.component.html',
    selector: 'simplon-first'
})
export class FirstComponent {
    /**
     * Pour qu'une variable soit accessible dans le template, il faut
     * la déclarer en propriété de la classe du Component (en public)
     */
    maVariable = 'bloup';
    imgSrc = 'https://ssl.gstatic.com/ui/v1/icons/mail/rfr/logo_gmail_lockup_default_1x.png';
    hidePara = false;

    togglePara() {
       this.hidePara = !this.hidePara;
    }

    test(truc) {
        console.log(truc);
    }
}