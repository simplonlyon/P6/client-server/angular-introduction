import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, mergeAll, toArray } from "rxjs/operators";
import { Event } from '../entity/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private url = 'http://localhost:3000/event/';

  constructor(private http:HttpClient) { }

  findAll(): Observable<Event[]> {
    return this.http.get<Event[]>(this.url)
    //Le pipe permet d'appliquer une suite d'opération sur l'observable
    //actuel qui créera un nouvel observable correspondant aux operations
            .pipe( 
              //L'opération map permet de modifier la valeur contenu à l'intérieur
              //de l'observable
              map(data => {
                //ici on boucle sur les events
                for(let event of data) {
                  //et on crée un objet Date JS à partir du timestamp stocké
                  //sur le server
                  event.date = new Date(event.date);
                }
                //Puis on retourne les données modifiées qui seront contenues
                //dans l'observable généré par le pipe
                return data;
              })
             );
   /*
   return this.http.get<Event[]>(this.url)
            .pipe( 
              mergeAll(),
              map(data => {
                data.date = new Date(data.date*1000);
                return data;
              }),
              toArray()

            );
            */
  }
  add(event:Event): Observable<Event> {
    let tmp = new Date(event.date);
    event.date=tmp.getTime();
    return this.http.post<Event>(this.url, event);
  }

  delete(id:number): Observable<any> {
    return this.http.delete<any>(this.url + id);
  }
}
