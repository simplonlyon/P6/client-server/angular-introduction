import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { ExoTemplateComponent } from './exo-template/exo-template.component';
import { StructureComponent } from './structure/structure.component';
import { ExoLiComponent } from './exo-li/exo-li.component';
import { Routes, RouterModule } from "@angular/router";
import { NotFoundComponent } from './not-found/not-found.component';
import { ObservableComponent } from './observable/observable.component';
import { EventComponent } from './event/event.component';

const routes:Routes = [
  {path: 'first', component: FirstComponent },
  {path: 'exo-li', component: ExoLiComponent },
  {path: 'exo-template', component: ExoTemplateComponent },
  {path: 'structure', component: StructureComponent },
  {path: 'observable', component: ObservableComponent },
  {path: 'event', component: EventComponent },
  /*
   * Le pathMatch full permet de dire qu'on veut appliquer la redirection
   * uniquement si l'url est '/' sans prendre en compte les 
   * éventuels prefix ou autre. C'est un cas particulier qu'on utilise preque
   * que dans le cas où on veut appliquer une route sur '/'
   */
  {path: '', redirectTo: '/first', pathMatch: 'full'},
  /*
  le path '**' est appelé wildcard, il correspond à n'importe quelle url.
  On s'en servira bien souvent pour faire une 404, un truc à charger
  dans le cas où l'url ne correspond à aucune route existante.
  /!\ Il est important de le mettre en dernier, sinon les autres routes ne seront
  pas prises en compte /!\
  */
  {path: '**', component: NotFoundComponent}
];


/**
 * Le NgModule représente un Module Angular, c'est à dire un groupe
 * de Components et de Services (équivalent d'un Bundle Symfony).
 * Tous les Components et Services du module doivent être déclaré
 * dans la partie declarations de celui ci.
 * Si le module utilise d'autres modules, il faudra déclarer ceux ci
 * dans la partie imports.
 * La partie bootstrap (rien à voir avec la librairie css) représente
 * le (ou les) component racine qui seront chargés automatiquement
 * au lancement de l'application.
 */
@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    ExoTemplateComponent,
    StructureComponent,
    ExoLiComponent,
    NotFoundComponent,
    ObservableComponent,
    EventComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
